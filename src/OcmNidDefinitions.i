/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* Cora Asset Tracker Protocol Schema
*  nid:        cora-asset-tracker
*  uuid:       ffaf1c40-72fc-437b-8737-9c6fbacce875
*  ver:        1.0.0.0
*  date:       2023-04-25T22:49:10.812Z
*  product: Cora Tracking CT10XX
* 
*  Copyright 2023 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_LocConfigPmode = 1,
	NID_ImpulseConfigDurwom = 2,
	NID_LocConfigPmodeDefault = 3,
	NID_TrackConfig = 4,
	NID_TemperatureCleartriggers = 5,
	NID_TemperatureConfigDtreport = 6,
	NID_SystemLogDisable = 7,
	NID_LocConfigPtech = 8,
	NID_ImpulseConfigRateactive = 9,
	NID_SystemReset = 10,
	NID_ImpulseConfigRange = 11,
	NID_SystemLog = 12,
	NID_TemperatureTriggerrpt = 13,
	NID_TrackMode = 14,
	NID_TemperatureConfig = 15,
	NID_SystemLogAlert = 16,
	NID_TemperatureReport = 17,
	NID_LocConfigPtechBle = 18,
	NID_TrackModeActive = 19,
	NID_ImpulseConfigThrshaccel = 20,
	NID_LocConfigPmodeMedium = 21,
	NID_TrackModeDisabled = 22,
	NID_SystemErase = 23,
	NID_LocConfigPtechWifible = 24,
	NID_TemperatureTriggerdef = 25,
	NID_PowerChargerCharging = 26,
	NID_SystemLogInfo = 27,
	NID_TrackConfigAcquire = 28,
	NID_SystemLogDetail = 29,
	NID_PowerChargerCharged = 30,
	NID_SystemConfig = 31,
	NID_PowerChargerDischarging = 32,
	NID_TrackModeEnabled = 33,
	NID_ImpulseConfigThrshwom = 34,
	NID_TemperatureConfigDtstats = 35,
	NID_ImpulseConfig = 36,
	NID_TemperatureConfigDtsampling = 37,
	NID_SystemLogAll = 38,
	NID_ImpulseConfigRatelpwr = 39,
	NID_PowerBattery = 40,
	NID_TemperatureConfigFlags = 41,
	NID_PowerChargerCritical = 42,
	NID_TrackConfigNomintvl = 43,
	NID_SystemConfigPollintvl = 44,
	NID_LocConfigPmodePrecise = 45,
	NID_LocConfigPtechWifi = 46,
	NID_LocConfig = 47,
	NID_PowerCharger = 48,
	NID_LocConfigPmodeCoarse = 49,
	NID_TrackConfigEmrintvl = 50,
	NID_ImpulseConfigFlags = 51,
	NID_LocConfigPtechAutomatic = 52,
	NID_ImpulseReport = 53,
	NID_TemperatureStatisticsrpt = 54,
	NID_ImpulseConfigThrshend = 55,
	NID_TrackConfigInactivity = 56
};
