/**
*  Name:  ConfigImpulseSensor.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2023 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
* This module implements the Impulse Sensor configuration.
**/
#include <OcmNidDefinitions.i>
#include <ImpulseSensorDefs.i>

//NID Command Map, by default these map to ped_tracker protocol spec.
//If your protocol is different, update the NID mappings.
const NIDMAP: {
	NM_IMPULSE_REPORT= _:NID_ImpulseReport,
	NM_IMPULSE_CONFIG= _:NID_ImpulseConfig,
	NM_IMPULSE_RATELP= _:NID_ImpulseConfigRatelpwr,
	NM_IMPULSE_RATEACT= _:NID_ImpulseConfigRateactive,
	NM_IMPULSE_RANGE= _:NID_ImpulseConfigRange,
	NM_IMPULSE_THRSHWOM= _:NID_ImpulseConfigThrshwom,
	NM_IMPULSE_DURWOM= _:NID_ImpulseConfigDurwom,
	NM_IMPULSE_THRSHACCEL= _:NID_ImpulseConfigThrshaccel,
	NM_IMPULSE_THRSHEND= _:NID_ImpulseConfigThrshend,
	NM_IMPULSE_FLAGS= _:NID_ImpulseConfigFlags
};

const  MotionSampling:	IMPULSE_RATELOWPWR	= MRATE_10HZ; //Low power sampling rate.
const  MotionSampling:	IMPULSE_ACTIVERATE	= MRATE_25HZ; //Active sampling rate.
const  MotionRange:		IMPULSE_RANGE		= MRNG_16G;	  //Motion range.
const  int:				IMPULSE_THRSH_WOM	= 50;	      //Wake on motion threshold in mGs.
const  int: 			IMPULSE_DURATION_WOM = 1;         //Duration of WOM required to trigger in sample period.
const  int:				IMPULSE_THRSH_ACCEL	 = 1400;  	  //Minimum Acceleration threshold for an impulse event.
const  int:				IMPULSE_THRSH_END	 = 500;		  //Minimum Acceleration threshold to end an impulse event.
const  int:				IMPULSE_THRSH_DELTAV = -1;		  //Minimum Velocity change for impulse event (-1) is not defined.
const  MsgPriority:		IMPULSE_MSGPRIORITY	= MP_high;	  //Message priority.	
const  MsgCategory:		IMPULSE_MSGCATEGORY	= MC_alert;	  //Message category.
const  _:  IMPULSE_FLAGS_DEFAULT = ISF_ENABLED; //Enable module by default.
// const			IMPULSE_IND_LED		= LED2;		//LED to use for indication.
// const			IMPULSE_IND_COUNT	= 10;		//Number of cycles to blink.
// const			IMPULSE_IND_ON		= 100;		//Blink on-time in milliseconds.
// const			IMPUSLE_IND_OFF		= 100;		//Blink off-time in milliseconds.

