# Vela Asset Tracker Application Release Notes

### V1.001 230502
1. Firmware reference v2.1.1.1a
   * Changed I2C memory bus from 100Khz to 400Khz.
   * Fixed accelerometer and i2c memory bugs.
   * Changed BVT LED behavior (manufacturing).
2. Platform version v1.3.1.4
### V1.000a  230425
1. Initial Release
2. Firmware reference v2.1.0.1a
4. Platform version v1.3.1.2


---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
