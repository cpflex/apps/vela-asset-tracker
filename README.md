# Vela Asset Tracker Application 
**Target: CT100X, CT102X Series Devices**<br/>
**Version: 1.001**<br/>
**Release Date: 2023/05/02**

Asset tracking application providing location tracking, temperature monitoring, and shock / impulse detection.   
Useful for a wide variety of application for assets both in and out of LoRaWAN coverage.  Message archive 
can store data while out of network and report as soon as communications are restored. This document highlights 
the general features and user interface functions. The application provides downlink commands
to set statistics reporting, sampling rates, and threshold alert notifications for impulse and temperature.

## Key Features  

1. Motion activated Impulse detection
   - Configurable: sampling rate, wake-up threshold, impulse detection thresholds, range (2,4,8,16 Gs).
   - Triggered Impulse events reports: 3 Axis + min / max acceleration, absolute/nominal velocity change, duration, and epoch.
   - Motion activated, sleeps while not in motion
2. Temperature monitoring
   - Statistics reporting: min., max., mean, std dev., epoch monitoring
   - Threshold (min., max.) alerts 
   - Remotely configurable threshold, reporting and sampling intervals
   - 1.2 degrees C uncertainty
3. WiFi/BLE location report when moving and stop points.
4. Charging, discharging, and battery status (every 5%) reports.
5. Battery Life TBD (> 3 months).
6. Battery status indicator
7. Sends initial location message immediately after reset.
 
# User Interface 

This application is meant to be used with both the Flex Tracker (CT100X) and Micro Tracker (CT102X) series of tags.   Certain UI capabilities will vary depending upon the device support. 

## Button Descriptions  (CT1000 only)

The following table summarizes the user interface for this application.  See the sections
below for more detailed information regarding each button action.  


| **Command**                         |     **Action**       |     **Button**     |  **Description**           |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Locate Now**                      | single (1) <br>quick press        | button #1          | Initiates an immediate location request  |  
| **Battery Status**                  | single (1) <br>quick press        | button #2          | Indicates battery level <br> [*See Battery Indicator*](#battery-indicator)  |  
| **Network Reset**                   | hold > 15<br>seconds              | any button or both | Network reset device      |
| **Factory Reset**                   | hold > 25<br>seconds              | any button or both | Resets the network and configuration to factory defaults for the application.     |
 
## LED Descriptions

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger pads
* **LED #2** - Lower LED, closest to charger pads (CT100X series only)

| **Description**                |                **Indication**               |   **LEDS**  |
|--------------------------------|:-------------------------------------------:|:-----------:|
| **Device Reset**               |       Green blink <br>three (3) times       |     both    |
| **Locate Initiated**           |        Green blink <br>two (2) times        |    LED #1   |
| **Battery Charging**           | Orange slow blink  every second<br> (one second on/off) |    LED #1   |
| **Battery Fully Charged**      | Solid green                                 |    LED #1   |
| **Invalid Input**              |  Red blink <br>three (3) times quickly      |    LED #1   |


See status indicators below for additional LED signaling when requesting battery status.
 
### Battery Indicator  (CT1000 only)

Quickly pressing Button #2 will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2   | % Charge    | Description                                     |
|----------|-------------|-------------------------------------------------|
|  1 red   |   < 10%     | Battery level is critical,  charge immediately. |
|  1 green |  15% - 30%  | Battery low  less than 30% remaining.           |
|  2 green |  30% - 50%  | Battery is about 40% charged.                    |
|  3 green |  50% - 70%  | Battery is about 60% charged.                    |
|  4 green |  70% - 90%  | Battery  is about 80% charged.                   |
|  5 green |  > 90%      | Battery is fully charged.                       |

# Application Integration
Application integration is discussed in detail. See [Application SDK](sdk/README.md)

# Building and Publishing

This section provide the steps for compiling and publishing the vibration-sensor app.

### Compiling the App
To compile the app use the compile executable with the following command line:

    compile <version: eg. v1.2.3.4> 

The command line requires a version tag.

### Compiling and Publishing.  
To build and publish both release and test apps simultaneously use the `compile_publish.bat`.  This ensures version and build configuration are handled consistently for the release and test targets.  The base compile and publish script is shown follow:

    compile_publish <version: eg. v1.2.3.4> 

This builds the application and publishes the updated image into the hub marking it as the latest build.

To compile and publish Alpha versions use `compile_publish_alpha.bat`.  This script produces a syslog variant build and does not mark the build ss the latest.

    compile_publish_alpha <version: eg. v1.2.3.4> 


---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
  
